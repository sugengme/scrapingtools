/**
 * @creator Sugeng. S (me@sugeng.me)
 *
 * Scraping data Perguruan Tinggi http://forlap.dikti.go.id/perguruantinggi
 *
 * Using PhantomJS & CasperJS
 *
 * casperjs scraping-pt.js
 *
 */

var fs 	   = require('fs');
var utils  = require('utils');
var casper = require('casper').create({
	verbose: true,
	logLevel: 'error',
	pageSettings: {
		loadPlugins: false,
		userAgent: "'Mozilla/5.0 (Windows NT 6.0) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.41 Safari/535.1'"
	}
});

var url     = "http://forlap.dikti.go.id/perguruantinggi";
var content = null;
var page    = null;
var data    = [];

function getData() {
	var rows = document.getElementsByClassName('ttop');

	return [].map.call(rows, function(cell) {
		return cell.innerText;
	});
}

function getNextPage() {
	return this.evaluate(function() {
		return $('.pagination .active').next().find('a').attr('href');
	});
}

function nextData(link) {
	this.start(link, function() {
	    nama = this.evaluate(getData);

	    nama.forEach(function(e) {
	    	data.push(e);
	    });
  	});
}

function loopPage() {
	if (page !== null || page !== '') {
		var url = '';

		this.echo("PROSES URL : " + page);

		this.thenClick('a[href="'+page+'"]', function() {
			page = getNextPage.call(this);

			nextData.call(this, this.getCurrentUrl());
		});

		saveToCsv(data);

	    data = [];

		this.run(loopPage);
	} else {
		saveToCsv(data);
		this.exit();
	}
}

function saveToCsv(data) {
	data.forEach(function(row) {
		fs.write('./perguruan-tinggi.txt', row+"\r\n", 'a');
	});
}

casper.options.viewportSize = { width: 1024, height: 1000 };

casper.start(url, function() {

	this.capture('captcha.png');
	console.log('Create captcha.txt now!! input captcha value from captcha.png than save');

	this.waitFor(function check() {
		return fs.exists('captcha.txt');
	},

	function then() {
		content = fs.read('captcha.txt');
		fs.remove('captcha.txt');

		this.evaluate(function(term) {
	    	document.querySelector('input[name="kode_pengaman"]').setAttribute('value', term);
		}, content);
	},

	function onTimeout() {
		console.log('habis');
	}, 60000);
});

casper.thenClick('input[value="Cari Perguruan Tinggi"]', function() {
	page = getNextPage.call(this);
	data = this.evaluate(getData);
});

casper.run(loopPage);